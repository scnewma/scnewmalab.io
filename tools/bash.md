# Bash

Below is my current bash profile.

```bash
if [ -s ~/.bashrc ]; then
    source ~/.bashrc
fi

if [ -f $(brew --prefix)/etc/bash_completion ]; then
    . $(brew --prefix)/etc/bash_completion
fi

function git_branch {
    if git rev-parse --git-dir >/dev/null 2>&1
        then echo -e " [$(git branch 2>/dev/null| sed -n '/^\*/s/^\* //p')]"
    else
        echo ""
    fi
}

NO_COLOR="\033[0m"
RED="\033[0;31m"
CYAN="\033[0;32m"
YELLOW="\033[0;33m"
BLUE="\033[0;34m"
PINK="\033[0;35m"

function git_color {
    local STATUS=`git status 2>&1`
    if [[ "$STATUS" == *'Not a git repository'* ]]
        then echo ""
    else
    if [[ "$STATUS" != *'working directory clean'* ]]
        then
        # red if need to commit
        echo -e "$RED"
    else
    if [[ "$STATUS" == *'Your branch is ahead'* ]]
        then
        # yellow if need to push
        echo -e "$YELLOW"
    else
        # else cyan
        echo -e "$CYAN"
    fi
    fi
    fi
}

export PS1="$USER \[$BLUE\]\W/\[\$(git_color)\]\$(git_branch) \[$PINK\]\n--|\[$NO_COLOR\] "

alias c='clear'
alias dev='cd ~/dev'
alias ls='ls -G'
alias cd..='cd ..'
alias grep='grep --color=auto'
alias curl='curl -v'
alias ll='ls -l'
```
