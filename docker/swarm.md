# Docker Swarm

> The information found on this page is sourced from [Mastering Docker by Russ McKendrick and Scott Gallagher](https://www.amazon.com/Mastering-Docker-Second-Russ-McKendrick-ebook/dp/B06X3X8JQ7/ref=sr_1_1?ie=UTF8&qid=1508036014&sr=8-1&keywords=mastering+docker)

With Docker Swarm, you can create and manager Docker clusters.  Swarm can be used to distribute containers across multiple hosts.  Swarm also has the ability to scale containers.

## Swarm Roles
### Swarm Manager
The host that is the central management point for all Swarm hosts.  This is where you issue all your commands to control those nodes.

> Swarm managers use the `Raft Consensus Algorithm` to maintain a consistent state across all of the manager nodes.  

### Swarm Worker
The hosts that run the Docker containers.  These are managed from the Swarm Manager.

## Create a Cluster
```
# create a manager node
docker-machine create -d virtualbox swarm-manager

# connect client to manager node
eval $(docker-machine env swarm-manager)

# create a couple of worker nodes
docker-machine create -d virtualbox swarm-worker01
docker-machine create -d virtualbox swarm-worker02

# bootstrap swarm manager
docker $(docker-machine config swarm-manager) swarm init \
  --advertise-addr $(docker-machine ip swarm-manager):2377 \
  --listen-addr $(docker-machine ip swarm-manager):2377
```

## Add Workers to Cluster
```
docker $(docker-machine config swarm-worker01) swarm join \
  $(docker-machine ip swarm-manager):2377 \
  --token SWMTKN-1-25nkcy9xtomw2ppycldpmxv9rpmbeq4rafe7gk9ersnocr0qta-9ayxtttvpuln44sxjwkuv46g3

docker $(docker-machine config swarm-worker02) swarm join \
  $(docker-machine ip swarm-manager):2377 \
  --token SWMTKN-1-25nkcy9xtomw2ppycldpmxv9rpmbeq4rafe7gk9ersnocr0qta-9ayxtttvpuln44sxjwkuv46g3
```

> You get the `—token` from the output of `swarm init`.  

## Listing Nodes
```
# make sure your swarm-master is the active node
docker node ls
```

## Get Node Information
```
docker node inspect swarm-manager --pretty
```

## Worker Promotion
> Use this if you need to do maintenance on your `swarm-manager` but don’t want to incur downtime.  

```
# this will still leave swarm-manager as the primary
docker node promote swarm-worker01

# demote the current manager so swarm-worker01 can take over
docker node demote swarm-manager

# you will need to update your client configuration to interact with cluster
eval $(docker-machine env swarm-worker01)
```

## Draining a Node
> Used to temporarily remove a node from our cluster so that we can perform maintenance.  
>   
> This will stop any new tasks from being executed against the node we are draining.  
```
docker node update --availability drain swarm-manager
```

## Add a Node back to Cluster
```
docker node update --availability active swarm-manager
```

## Services
The `service` command is a way of launching containers that take advantage of the Swarm cluster.

```
docker service create --name cluster \
  --constraint "node.role == worker" \
  -p:80:80/tcp russmckendrick/cluster
```

### List services
```
docker service ls
```

### Get more information
```
docker service inspect cluster --pretty
```

### Scaling
> This well balance between worker nodes by default  

```
# scale to six instances of cluster container
docker service scale cluster=6
```

### Removing Services
```
docker service rm cluster
```

## Stacks
Stacks allow you to define `docker-compose.yml` files to create your services and distribute them across your swarm.

> This example accomplishes the same thing as the `Services` example above.  

```
# docker-compose.yml
version: "3"
services:
  cluster:
    image: russmckendrick/cluster
    ports:
      - "80:80"
    deploy:
      replicas: 6
      restart_policy:
        condition: on-failure
      placement:
        constraints:
          - node.role == worker

# deploy the stack
docker stack deploy --compose-file=docker-compose.yml cluster
```

### Stack Details
```
# list stacks
docker stack ls

# service details
docker stack services cluster

# list containers on stack
docker stack ps cluster
```

### Removing Stack
```
docker stack rm cluster
```
