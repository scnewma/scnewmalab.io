# Docker Machine

> The information found on this page is sourced from [Mastering Docker by Russ McKendrick and Scott Gallagher](https://www.amazon.com/Mastering-Docker-Second-Russ-McKendrick-ebook/dp/B06X3X8JQ7/ref=sr_1_1?ie=UTF8&qid=1508036014&sr=8-1&keywords=mastering+docker)

Docker Machine can be used to easily launch and bootstrap Docker hosts targeting various platforms, including locally or in a cloud environment.  You can control your Docker hosts with it as well.

## Create a new Docker Machine
The below command creates a virtual machine with docker on it.  Docker Machine supports a lot of different drivers.  Virtualbox is used below as an example.

```
docker-machine create --driver virtualbox <machine-name>
```

> This command creates the machine and starts it up.  It also sets up your machine with everything it will need to connect to the created machine.  

### View the machine’s environment

```
docker-machine env <machine-name>
```

### Point CLI to Docker Machine
You can run the below command to update your local environment variables to update to the environment of the Docker Machine.  This allows you to be able to interact with the Docker Machine as if it’s your local environment.

```
eval $(docker-machine env <machine-name>)
```

> If you want to target your local machine again, run `eval $(docker-machine env -u)`

## List Currently Configured Docker Hosts
```
# The output with the '*' is the what your client is interacting with
docker-machine ls
```

### Active Host
You can also determine the active docker host by running:
```
docker-machine active
```

## SSH into Docker Host
SSHing to your Docker host is useful if you need to install additional software or configuration outside of Docker Machine.
```
docker-machine ssh <machine-name>
```

## Common Commands
### Find Machine IP
```
docker-machine ip <machine-name>
```

### Docker Machine State
```
docker-machine start <machine-name>

docker-machine stop <machine-name>

docker-machine restart <machine-name>

docker-machine rm <machine-name>
```

### Docker Machine Details
```
docker-machine inspect <machine-name>

docker-machine config <machine-name>

docker-machine status <machine-name>

docker-machine url <machine-name>
```

You should use the `docker-machine config <machine-name>` command if you want to scope your Docker client to point to a given Docker Machine.
For example:
```
# check the docker version of a docker machine
docker $(docker-machine config <machine-name>) version
```

## Different Base OS
You can launch a Docker Machine with a different base image than the default with a simple flag.

Here is an example of using the RancherOS image on a local VirtualBox hypervisor:
```
docker-machine create -d virtualbox \
   --virtualbox-boot2docker-url https://releases.rancher.com/os/latest/rancheros.iso
   docker-rancher
```
> One downside to this is that you will not be able to use the `docker-machine upgrade` command on any machines created with a different base image.
