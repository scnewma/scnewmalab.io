# Docker Containers

> The information found on this page is sourced from [Mastering Docker by Russ McKendrick and Scott Gallagher](https://www.amazon.com/Mastering-Docker-Second-Russ-McKendrick-ebook/dp/B06X3X8JQ7/ref=sr_1_1?ie=UTF8&qid=1508036014&sr=8-1&keywords=mastering+docker)

## Common Commands
### ls
```
# list containers (running only)
docker container ls

# list all containers
docker container ls -a
```

### attach
Use attach to interact with a running container process.

```
# attaches to the container
# Ctrl-C will kill the process
docker container attach <container-name>

# to make Ctrl-C not kill process
docker container attach --sig-proxy=false <container-name>
```

### exec
This spawns a second process within the container that you can interact with.

```
# execute a given command
docker container exec <container-name> <cmd>

# get an interactive shell
docker container exec -i -t <container-name> /bin/bash
```

> While it is extremely useful as you can interact with the container as if it were a virtual machine, you should not make any changes to your containers as they are running.  
> It is more than likely that those changes will not persist and will be lost when your container is removed.  

### logs
Allows you to interact with `stdout` stream of your containers.

```
# retrieve the 'n' latest logs
docker container logs --tail 5 <container-name>

# view logs in real time
docker container logs -f <container-name>

# view logs since
docker container logs --since 2017-06-24T15:00 <container-name>

# add -t to show full container timestamp
docker container logs --since 2017-06-24T15:00 -t <container-name>
```

> The time in the “view logs since” example is based on local time although the timestamps that are shown in the output are shown in container time (which is probably UTC).  

### top
Lists the processes running within the container you specify.

```
docker container top <container-name>
```

### stats
Provides real-time information on either the specified container or, if you don’t pass a container name or ID, all running containers.

```
docker container stats <container-name>
```

> The output is real-time only.  Docker does not track the previous stats like the `logs` command does.  

## Resource Limits
```
# start a container with limited resources
docker container run -d --name <container-name> --cpu-shares 512 --memory 128M -p 8080:80 <image>
```

### Update Resource Limits on a Running Container
```
# this will fail because of the memoryswap limit
docker container update --cpu-shares 512 --memory 128M <container-name>
```

Find out what the `memoryswap` is currently set to with `inspect`:
```
# this will probably show a 'memoryswap' of 0
# 0 is the default value, which means unbounded
docker container inspect <container-name> | grep -i 'memory'
```

```
# update 'memory' and 'memoryswap'
docker container update --cpu-shares 512 --memory 128M --memory-swap 256M <container-name>
```

> By default when you set `--memory` as part of the `run` command, Docker will set the `--memory-swap` size to be twice that of `—memory`.  

## Container States
### Pause and unpause
Suspends the process using the `cgroups` freezer.  With the `cgroups` freezer, the process is unaware it has been suspended, meaning that it can be resumed.

```
docker container pause <container>

docker container unpause <container>
```

> These commands are useful if you need to freeze the state of a container; maybe one of your containers is going haywire and you need to do some investigation later but don’t want it to have a negative impact on your other running containers.  

### Stop, start, restart, and kill
```
# start a stopped container
docker container start <container>

# stop a running container (SIGTERM before SIGKILL)
# default SIGTERM timing is 10s
docker container stop <container>

# you can override the default SIGTERM timeout
docker container stop -t <sec> <container>

# restart combines stop and start
docker container restart [-t <sec>] <container>

# send a SIGKILL immediately
docker container kill <container>
```

### Removing Containers
#### prune
Removes all exited containers
```
docker container prune
```

#### rm
Removes a specific container
```
docker container rm <container>

# force remove if running (stop then rm)
docker container rm -f <container
```

## Misc Commands
### create
The `create` command is pretty similar to the `run` command, except that it does not start the container, but instead prepares and configures one:
```
docker container create --name <name> [OPTIONS] <image>
```

### port
The `port` command displays the port along with any port mappings for the container.
```
docker container port <container>
```
> Ports are also listed in `docker container ls`  

### diff
The `diff` command prints a list of all of the files that have been added or changed since the container started to run—so basically, a list of the differences on the filesystem between the image we used to launch the container and now.
```
docker container diff <container>
```

## Networking
When you create a network and associate containers to it, then the containers on that network will be able to interact with one another.

### Create a new network:
```
docker network create <network-name>
```

### Run a container on the network:
```
docker container run -d --name <container> --network <network-name> <image>
```

### Run a container on the network with an alias:
> this is useful if a consuming container is expecting one name, but that container name is already taken  
```
docker container run -d --name <container> --network <network-name> --network-alias <alias> <image>
```

### Display all networks:
```
docker network ls
```

### Retrieve network configuration:
```
docker network inspect <network-name>
```

### Remove unused networks:
```
docker network prune
```

## Volumes
You can utilize volumes in your `Dockerfile` with the `VOLUME` and `WORKDIR` commands:
```
VOLUME /data
WORKDIR /data
```

### Map a created volume to your docker container:
```
docker container run -d --name <name> -v <vol-id>:<path> <image>

# example
docker container run -d --name redis -v 3ba77cf42da36840691665179580eb1247df6158b9a760568ac8b295f874d65c:/data --network moby-counter redis:alpine
```

### View created volumes:
```
docker volume ls
```

### Create a volume:
```
docker volume create <volume-name>
```

### View volume information:
```
docker volume inspect <volume-name>
```

### Remove unused volumes:
```
docker volume prune
```
