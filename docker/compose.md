# Docker Compose

> The information found on this page is sourced from [Mastering Docker by Russ McKendrick and Scott Gallagher](https://www.amazon.com/Mastering-Docker-Second-Russ-McKendrick-ebook/dp/B06X3X8JQ7/ref=sr_1_1?ie=UTF8&qid=1508036014&sr=8-1&keywords=mastering+docker)

Docker Compose is a way to use Docker to orchestrate creating multiple containers, networks, and so on.  This enables you to deploy your “entire stack” easily.

## docker-compose.yml
The orchestration is defined in a docker-compose.yml file.  Here is an example of a two-container application.  _The moby-counter application uses redis:alpine as the backing store_

```
version: "3"

services:
  redis:
    image: redis:alpine
    volumes:
      - redis_data:/data
    restart: always
  mobycounter:
    depends_on:
      - redis
    image: russmckendrick/moby-counter
    ports:
      - "8080:80"
    restart: always

volumes:
  redis_data
```

> You can launch the application by running `docker-compose up` in a folder with a `docker-compose.yml` file.  

### What all did this do?
1. Create a network called `<prefix>_default` _we didn’t have to tell docker-compose to do that_
2. Create a volume called `<prefix>_redis_data`
3. Launch two containers `<prefix>_redis_1` and `<prefix>_mobycounter_1`

> The `<prefix>` comes from the folder name where `docker-compose.yml` is located  

## Helpful `docker-compose.yml` features
### build
You can nest `build` under your service definition.  This should point to a directory that hosts the `Dockerfile` for the container that you want to run.
```
...
  vote:
    # there should be a `Dockerfile` in vote/
    build: ./vote
```

### command
Nested under your service definition.  This is the `command` that you want your docker container to run on startup.

```
...
  vote:
    command: python app.py
```

## Commands
When running the following commands, Docker Compose will only be aware of the containers defined in the service section of your `docker-compose.yml` file.
### up
```
# -d for detached
docker-compose up [-d]
```

### ps
```
docker-compose ps
```

### config
```
# -q shorthand for quiet (will only print errors)
docker-compose config [-q]
```

### pull, build, and create
```
# pull any images found in docker-compose.yml
docker-compose pull

# execute build instructions in docker-compose.yml
docker-compose build

# create but do no launch containers
docker-compose create
  [--force-recreate]
  [--no-recreate] # do not recreate if container exists
  [--no-build] # never build, even if missing
  [--build] # build images before creating containers
```

### start, stop, restart, prune, unpause
These commands work exactly the same way as their `docker container` counterparts except they effect all of the containers.
```
docker-compose start
docker-compose stop
docker-compose restart
docker-compose pause
docker-compose unpause

# you can target a single service if needed
docker-compose pause <service>
```

### top, logs, and events
```
# display top processes for each container
docker-compose top [<service-name>]

# -f streams logs in real time
docker-compose logs [-f] [<service-name>]

docker-compose events
```

### exec and run
```
docker-compose exec <service> <command>

docker-compose run <service> <command>
```

### scale /deprecated/
The `scale` command will take the service you pass and scale it to the number you define.
```
# scale <service> to 3 instances
docker-compose scale <service>=3

# use --scale option on up instead
docker-compose up -d --scale <service>=3
```

### kill, rm, and down
```
# does not wait for containers to stop gracefully
docker-compose kill

# removes any containers with the state of exited
docker-compose rm

# removes containers, networks, volumes, images, and so on
docker-compose down
```
